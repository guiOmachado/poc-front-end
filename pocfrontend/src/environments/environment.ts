// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBus:'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o',
  apiLotacao: 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l',
  apiItinerario: 'http://www.poatransporte.com.br/php/facades/process.php?a=il&p='
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
