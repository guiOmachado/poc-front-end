import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LinhaDeOnibus } from '../models/Linha';
import { catchError, retry, tap } from 'rxjs/operators'
import { Lotacao } from '../models/Lotacao';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiBus = environment.apiBus;
  private apiLotacao = environment.apiLotacao;
  private apiItinerario = environment.apiItinerario;

  constructor(private http: HttpClient) {
  }

  httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json')} 

  public getLinhas(): Observable<LinhaDeOnibus[]> {
    return this.http.get<LinhaDeOnibus[]>(`${this.apiBus}`).pipe(
      tap(dados => console.log('Retornando todos as linhas'),catchError(this.handleError)));
  }

  public getItinerario(id: number): Observable<LinhaDeOnibus[]> {
    return this.http.get<any>(`${this.apiItinerario+id}`).pipe(
      tap(dados => console.log('Retornando todos os itinerarios'),catchError(this.handleError)));
  }

  public getLotacao(): Observable<LinhaDeOnibus[]> {
    return this.http.get<Lotacao[]>(`${this.apiLotacao}`).pipe(
      tap(dados => console.log('Retornando todos as lotacoes'),catchError(this.handleError)));
    }  

  public getOption(selecao: boolean)
  {
      return selecao;
  }

  public handleError() {
    console.log("DEU ERRO")
    return (erro: HttpErrorResponse) => {
      let msg = '';
      if (erro.error instanceof ErrorEvent) {
        //falha no clado cliente
        console.log(erro.error.message)
        msg = `Falha no cliente: ${erro.error.message}`;
      } else {
        //falha no lado servidor
        console.log(erro.message)
        msg = `Falha no servidor: ${erro.status}, ${erro.statusText}, ${erro.message}`;
      }
      console.log("ERRO => " + msg);
      return throwError(msg);
    }
  }


}
