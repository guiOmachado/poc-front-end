import { importType } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit } from '@angular/core';
import { LatLong } from 'src/app/models/LatLong';
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  @Input() latitudeOrigin: number;
  @Input() longitudeOrigin: number;
  @Input() latitudeDestination: number;
  @Input() longitudeDestination: number;
  @Input() waypoint: LatLong[];


  displayDirections = true;
  zoom = 15;

  constructor() {
    this.latitudeOrigin = 0;
    this.longitudeOrigin = 0;
    this.latitudeDestination = 0;
    this.longitudeDestination = 0;
    this.waypoint = [];
  }

  ngOnInit(): void {
  }
}

