import { Directive, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';
import { LatLong } from './models/LatLong';

// You can use any other interface for origin and destination, but it must contain latlng data
export interface ILatLng {
  latitude: number;
  longitude: number;
}

// the will keep typescript from throwing errors w.r.t the google object
declare var google: any;
let markers: google.maps.Marker[] = [];
let rota: google.maps.Polyline;

@Directive({
  selector: '[appDirectionsMap]'
})
export class DirectionsMapDirective implements OnInit, OnChanges {
  @Input() origin!: ILatLng;
  @Input() destination!: ILatLng;
  @Input() waypoint!: Object;
  @Input() showDirection!: boolean;


  // We'll keep a single google maps directions renderer instance so we get to reuse it.
  // using a new renderer instance every time will leave the previous one still active and visible on the page
  private directionsRenderer: any;


  // We inject AGM's google maps api wrapper that handles the communication with the Google Maps Javascript
  constructor(private gmapsApi: GoogleMapsAPIWrapper) { }

  ngOnInit() {
    this.drawDirectionsRoute();
  }

  drawDirectionsRoute() {

    var teste = Object.keys(this.waypoint).map(i => this.waypoint[i]);

    var teste2 = Object.values(teste)[0];
    console.log("ARRAY DE COM VALORES DO TESTE2 NA POSICAO 0", teste2);

    this.gmapsApi.getNativeMap().then(map => {
      if (!this.directionsRenderer) {
        // if you already have a marker at the coordinate location on the map, use suppressMarkers option
        // suppressMarkers prevents google maps from automatically adding a marker for you
        this.directionsRenderer = new google.maps.DirectionsRenderer({ suppressMarkers: true });
      }
      const directionsRenderer = this.directionsRenderer;

      const waypts: google.maps.LatLng[] = [];

      for (let i = 0; i < teste2.length; i++) {
        waypts.push(
          new google.maps.LatLng(teste2[i].lat, teste2[i].lng),
        );
      }





      //console.log("array de waypoints dentro do for ", this.waypoint[i]);
      //waypts[i].location = new google.maps.LatLng();


      console.log("array de waypoints ", waypts);
      if (this.showDirection && this.destination) {
        //deleta os markers anteriores quando mudamos a rota
        deletePath();
        rota.setMap(map);
        const directionsService = new google.maps.DirectionsService;
        /* var iconCliente = '/assets/casa.PNG';
        markers.push(new google.maps.Marker({
          position: { lat: this.destination.latitude, lng: this.destination.longitude },
          title: "Cliente",
          icon: iconCliente,
          //map: map,
        }));
  
        var iconEntregador = '/assets/iconemotoboy.png';
        markers.push(new google.maps.Marker({
          position: { lat: this.waypoint.latitude, lng: this.waypoint.longitude },
          title: "Entregador",
          icon: iconEntregador,
          //map: map,
        }));
  
        var iconPanvel = '/assets/farmacia.PNG';
        markers.push(new google.maps.Marker({
          position: { lat: this.origin.latitude, lng: this.origin.longitude },
          title: "Panvel",
          icon: iconPanvel,
          //map: map,
        })); */


        //seta os markers da rota
        //setMapOnAll(map);
        rota = new google.maps.Polyline({
          path: waypts,
          geodesic: true,
          strokeColor: "#FF0000",
          strokeOpacity: 1.0,
          strokeWeight: 3,
        });

        rota.setMap(map);
        directionsRenderer.setMap(map);
        
        directionsRenderer.addListener("directions_changed", () => {
          computeTotalDistance(directionsRenderer.getDirections());
        });


        directionsService.route({
          origin: new google.maps.LatLng(this.origin.latitude, this.origin.longitude),
          destination: new google.maps.LatLng(this.destination.latitude, this.destination.longitude),
          optimizeWaypoints: false,
          travelMode: 'DRIVING'

        }, (response: any, status: string) => {



          if (status === 'OK') {
            directionsRenderer.setDirections(response);

          } else {
            console.log('Directions request failed due to ' + status);
          }
        });

      }

    });


  }



  ngOnChanges(changes: SimpleChanges) {
    if (changes.destination || changes.showDirection) {
      // this checks if the show directions input changed, if so the directions are removed
      // else we redraw the directions
      if (changes.showDirection && !changes.showDirection.currentValue) {
        if (this.directionsRenderer !== undefined) { // check this value is not undefined  

          this.directionsRenderer.setDirections({ routes: [] });
          return;
        }
      } else {
        this.drawDirectionsRoute();
      }
    }
  }
}

function setMapOnAll(map: google.maps.Map | null) {
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

function clearMarkers() {
  setMapOnAll(null);
}

function deletePath() {
  rota = new google.maps.Polyline({
    path: [],
    geodesic: true,
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 3,
  });;
}

function deleteMarkers() {
  clearMarkers();
  markers = [];
}

function computeTotalDistance(result: google.maps.DirectionsResult) {
  let total = 0;
  let time = 0;
  const myroute = result.routes[0];

  for (let i = 0; i < myroute.legs.length; i++) {
    total += myroute.legs[i].distance.value;
    time += myroute.legs[i].duration.value;
  }
  total = total / 1000;
  time = time / 60;
}