import { ILatLng } from "../directions-map.directive";


export interface Itinerario {
    idlinha: number,
    nome: string,
    codigo: number,
    latLong?: Array<ILatLng>   
}