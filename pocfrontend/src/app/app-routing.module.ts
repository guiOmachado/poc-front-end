import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaLinhasComponent } from './components/lista-linhas/lista-linhas.component';

const routes: Routes = [
 { path: '', component: ListaLinhasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
