import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService } from './service/api.service';
import { ListaLinhasComponent } from './components/lista-linhas/lista-linhas.component';
import { NgbButtonsModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { MapaComponent } from './mapa/mapa.component';
import { DirectionsMapDirective } from './directions-map.directive';
import { ListaOnibusComponent } from './components/lista-onibus/lista-onibus.component';
import { ListaLotacaoComponent } from './components/lista-lotacao/lista-lotacao.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {MatToolbarModule} from '@angular/material/toolbar';


@NgModule({
  declarations: [
    AppComponent,
    ListaLinhasComponent,
    MapaComponent,
    DirectionsMapDirective,
    ListaOnibusComponent,
    ListaLotacaoComponent,
    HeaderComponent,
    FooterComponent    
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
    CommonModule,
    MatSlideToggleModule,
    NgbButtonsModule,
    MatToolbarModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCK2awmKvTE7fJ71oRdEAhLoOUKkhDJd9Q'
    })    
  ],
  exports:[MapaComponent,ListaOnibusComponent,ListaLotacaoComponent],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
