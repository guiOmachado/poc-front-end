import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ILatLng } from 'src/app/directions-map.directive';
import { Itinerario } from 'src/app/models/Itinerario';
import { LinhaDeOnibus } from 'src/app/models/Linha';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-lista-onibus',
  templateUrl: './lista-onibus.component.html',
  styleUrls: ['./lista-onibus.component.css']
})
export class ListaOnibusComponent implements OnInit {

  currentlyClickedCardIndex = 0;
  itinerario: Itinerario;
  currentlyClickedOnibus = {} as LinhaDeOnibus;
  nome: string = "";
  latLong: ILatLng[] = [];
  i = 0;
  linhas: LinhaDeOnibus[] = [];
  closeResult = '';
  constructor(private router: Router, private route: ActivatedRoute, private service: ApiService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getListaLinhasBus();
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public getListaLinhasBus() {

    this.service.getLinhas().subscribe((linhas: LinhaDeOnibus[]) => {
      this.setLinhas(linhas);
    });
  }

  public setLinhas(linhasDeOnibus: LinhaDeOnibus[]) {
    this.linhas = linhasDeOnibus;
    console.log("ONIBUS", this.linhas);
  }

  public setItinerarios(itinerario: Itinerario) {
    this.itinerario = itinerario;

    while (true) {
      if (!itinerario[this.i]) {
        console.log(this.i);
        break;
      }
      this.latLong[this.i] = itinerario[this.i];

      this.i++;
    }
    this.itinerario.latLong = this.latLong;

    this.i = 0;
    console.log("ITINERARIO COM LATLONG", this.latLong);
  }

  public checkIfCardIsClicked(cardIndex: number): boolean {
    return cardIndex == this.currentlyClickedCardIndex;
  }

  public setcurrentlyClickedCardIndexB(cardIndex: number): void {
    console.log("LINHA CLICADA")
    console.log(cardIndex)
    this.currentlyClickedCardIndex = cardIndex;
    this.currentlyClickedOnibus = this.linhas.find(x => x.id === this.currentlyClickedCardIndex)!;
    this.getListaLinhasItinerarios(this.currentlyClickedOnibus.id);
    console.log("ONIBUS", this.currentlyClickedOnibus)
  }

  public getListaLinhasItinerarios(idLinha: number) {
    this.service.getItinerario(idLinha).subscribe((itinerario: any) => {
      this.setItinerarios(itinerario);
    });
  }
}
