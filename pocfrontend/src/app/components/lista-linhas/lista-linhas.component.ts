import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Itinerario } from 'src/app/models/Itinerario';
import { LinhaDeOnibus } from 'src/app/models/Linha';
import { Lotacao } from 'src/app/models/Lotacao';
import { ApiService } from 'src/app/service/api.service';
import { LatLong } from "src/app/models/LatLong";
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ILatLng } from 'src/app/directions-map.directive';


@Component({
  selector: 'app-lista-linhas',
  templateUrl: './lista-linhas.component.html',
  styleUrls: ['./lista-linhas.component.css']
})


export class ListaLinhasComponent implements OnInit {
  
  currentlyClickedCardIndex = 0;
  currentlyClickedOnibus = {} as LinhaDeOnibus;
  currentlyClickedLotacao = {} as Lotacao
  linhas: LinhaDeOnibus[] = [];
  lotacoes: Lotacao[] = [];
  latLong: ILatLng[] = [];
  itinerario: Itinerario;
  nome: string = "";
  mostraOnibus = false;
  i = 0;
  closeResult = '';


  constructor(private router: Router, private route: ActivatedRoute, private service: ApiService, private modalService: NgbModal) { }

  ngOnInit(): void { 
     
  }

  public changeList(opcao: boolean) {
    if (opcao) {
      this.mostraOnibus = opcao;
    }
    else {
      this.mostraOnibus = opcao;     
    }
    this.currentlyClickedCardIndex = 0;
  }

   open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  

}
