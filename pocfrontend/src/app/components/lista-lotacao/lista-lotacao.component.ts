import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ILatLng } from 'src/app/directions-map.directive';
import { Itinerario } from 'src/app/models/Itinerario';
import { Lotacao } from 'src/app/models/Lotacao';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-lista-lotacao',
  templateUrl: './lista-lotacao.component.html',
  styleUrls: ['./lista-lotacao.component.css']
})
export class ListaLotacaoComponent implements OnInit {
  currentlyClickedCardIndex = 0;
  lotacoes: Lotacao[] = [];
  latLong: ILatLng[] = [];
  itinerario: Itinerario;
  @Input() nome: string = "";
  currentlyClickedLotacao = {} as Lotacao;
  i = 0;
  closeResult = '';
  constructor(private router: Router, private route: ActivatedRoute, private service: ApiService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getListaLinhasLotacao();
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public getListaLinhasLotacao() {
    this.service.getLotacao().subscribe((lotacoes: Lotacao[]) => {
      this.setLotacao(lotacoes);
    });
  }

  public setLotacao(lotacao: Lotacao[]) {
    this.lotacoes = lotacao;
    console.log("LOTACOES", this.lotacoes);
  }

  public getListaLinhasItinerarios(idLinha: number) {
    this.service.getItinerario(idLinha).subscribe((itinerario: any) => {
      this.setItinerarios(itinerario);
    });
  }

  public setItinerarios(itinerario: Itinerario) {
    this.itinerario = itinerario;

    while (true) {
      if (!itinerario[this.i]) {
        console.log(this.i);
        break;
      }
      this.latLong[this.i] = itinerario[this.i];

      this.i++;
    }
    this.itinerario.latLong = this.latLong;

    this.i = 0;
    console.log("ITINERARIO COM LATLONG", this.latLong);
  }

  public checkIfCardIsClicked(cardIndex: number): boolean {
    return cardIndex == this.currentlyClickedCardIndex;
  }

  public setcurrentlyClickedCardIndexB(cardIndex: number): void {
    console.log("LINHA CLICADA")
    console.log(cardIndex)
    this.currentlyClickedCardIndex = cardIndex;
    this.currentlyClickedLotacao = this.lotacoes.find(x => x.id === this.currentlyClickedCardIndex)!;
    this.getListaLinhasItinerarios(this.currentlyClickedLotacao.id);
    console.log("LOTAÇÃO",this.currentlyClickedLotacao)
  }
  
}




